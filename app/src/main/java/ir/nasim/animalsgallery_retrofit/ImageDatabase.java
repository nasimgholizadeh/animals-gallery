package ir.nasim.animalsgallery_retrofit;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import ir.nasim.animalsgallery_retrofit.Interface.ImageDao;
import ir.nasim.animalsgallery_retrofit.Model.Images;

@Database(entities = {Images.class}, version = 1)
public abstract class ImageDatabase extends RoomDatabase {
    public abstract ImageDao imageDao();
}
