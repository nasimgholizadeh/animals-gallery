package ir.nasim.animalsgallery_retrofit.Interface;

import ir.nasim.animalsgallery_retrofit.Model.Animals;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiRequest {
    //fox request
    String FOX_URL ="https://randomfox.ca/";
    @GET("floof/?ref=public-apis")
    Call<Animals> getFox();

    //cat request
    String CAT_URL ="https://aws.random.cat/";
    @GET("meow?ref=public-apis")
    Call<Animals> getCat();

    //dog request
    String DOG_URL ="https://dog.ceo/";
    @GET("api/breeds/image/random")
    Call<Animals> getDog();
}
