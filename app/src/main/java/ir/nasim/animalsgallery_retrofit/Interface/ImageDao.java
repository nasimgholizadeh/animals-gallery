package ir.nasim.animalsgallery_retrofit.Interface;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ir.nasim.animalsgallery_retrofit.Model.Images;

@Dao
public interface ImageDao {
    @Query("select * from Images")
    List<Images> getAll();

    @Insert
    void insertAll(List<Images> imagesList);
}
