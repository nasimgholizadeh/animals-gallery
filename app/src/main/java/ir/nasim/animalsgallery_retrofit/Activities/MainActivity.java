package ir.nasim.animalsgallery_retrofit.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import ir.nasim.animalsgallery_retrofit.Connection.NetworkConnectionDetector;
import ir.nasim.animalsgallery_retrofit.ImageDatabase;
import ir.nasim.animalsgallery_retrofit.Interface.ApiRequest;
import ir.nasim.animalsgallery_retrofit.Model.Animals;
import ir.nasim.animalsgallery_retrofit.Model.Images;
import ir.nasim.animalsgallery_retrofit.R;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.senab.photoview.PhotoView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private Context context;

    private Animals animals;

    Bitmap bitmap;

    public static ImageDatabase database;

    private Button btnFox, btnCat, btnDog, btnSave, btnAddFavourites, btnFavourites;
    private PhotoView photoView;
    private MaterialProgressBar progressBar;
    private Toolbar toolbar;

    private NetworkConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getStoragePermission();
        setContentView(R.layout.activity_main);

        initViews();

        setSupportActionBar(toolbar); //toolbar

        context=this;

        //roomDB
        database= Room.databaseBuilder(context,ImageDatabase.class,"AnimalsGallery").allowMainThreadQueries().build();

        connectionDetector=new NetworkConnectionDetector(context);

        //disable save and add favourite button
        btnSave.setEnabled(false);
        btnAddFavourites.setEnabled(false);

        btnFavourites.setOnClickListener(view -> {
            Intent intent=new Intent(context,FavouritesActivity.class);
            startActivity(intent);
        });

        btnAddFavourites.setOnClickListener(view -> {
            List<Images> imagesList=new ArrayList<>();
            imagesList.add(new Images(photoViewToByte(photoView)));
            //insert
            database.imageDao().insertAll(imagesList);
            //show
            List<Images> getImages;
            getImages=database.imageDao().getAll();
            Log.i(TAG, "inserted: "+getImages);
            Toasty.success(MainActivity.this,"Added to favourites!",Toast.LENGTH_LONG).show();
        });
        
        btnFox.setOnClickListener(view -> {
            btnSave.setEnabled(true);
            btnAddFavourites.setEnabled(true);
            getFoxImage();
        });

        btnCat.setOnClickListener(view -> {
            btnSave.setEnabled(true);
            btnAddFavourites.setEnabled(true);
            getCatImage();
        });

        btnDog.setOnClickListener(view -> {
            btnSave.setEnabled(true);
            btnAddFavourites.setEnabled(true);
            getDogImage();
        });

        btnSave.setOnClickListener(view -> {
            getStoragePermission(); //get read/write permission

            bitmap = ((BitmapDrawable) photoView.getDrawable()).getBitmap();

            String time = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(System.currentTimeMillis());
            File path = Environment.getExternalStorageDirectory();
            File dir = new File(path + "/DCIM/Animals");
            dir.mkdirs(); //create folder
            Log.i(TAG, "dir: "+dir.mkdirs());
            String imageName = time + ".jpg";
            File file = new File(dir, imageName);
            OutputStream out;
            try {
                out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
                Toasty.success(MainActivity.this,"Saved.",Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toasty.error(MainActivity.this,"An error occurred in storage!!!",Toast.LENGTH_LONG).show();
            }
            //show image in gallery
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(file));
            sendBroadcast(intent);
        });
    }

    //function for get fox image
    private void getFoxImage() {
        //check network connectivity
        if (connectionDetector.isConnected()) {
            //show progress bar
            progressBar.setVisibility(View.VISIBLE);
            //create retrofit instance
            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(ApiRequest.FOX_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiRequest request=retrofit.create(ApiRequest.class);
            Call<Animals> call=request.getFox();
            call.enqueue(new Callback<Animals>() {
                @Override
                public void onResponse(Call<Animals> call, Response<Animals> response) {
                    if (response.isSuccessful() && response.body()!=null){
                        animals = response.body();
                        String imageUrl=animals.getFoxUrl();
                        //load image url and show in imageView with picasso
                        Picasso.get().load(imageUrl).into(photoView);
                        //hide progress bar
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<Animals> call, Throwable t) {
                    Toast.makeText(MainActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                }
            });
        }
        else
            Toasty.error(context, "You're offline!", Toast.LENGTH_SHORT, true).show();
    }

    //function for get cat image
    private void getCatImage() {
        if (connectionDetector.isConnected()) {
            progressBar.setVisibility(View.VISIBLE);
            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(ApiRequest.CAT_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiRequest request=retrofit.create(ApiRequest.class);
            Call<Animals> call=request.getCat();
            call.enqueue(new Callback<Animals>() {
                @Override
                public void onResponse(Call<Animals> call, Response<Animals> response) {
                    if (response.isSuccessful() && response.body()!=null){
                        animals = response.body();
                        String imageUrl=animals.getCatUrl();
                        Picasso.get().load(imageUrl).into(photoView);
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<Animals> call, Throwable t) {
                    Toast.makeText(MainActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                }
            });
        }
        else
            Toasty.error(context, "You're offline!", Toast.LENGTH_SHORT, true).show();
    }

    //function for get dog image
    private void getDogImage() {
        if (connectionDetector.isConnected()) {
            progressBar.setVisibility(View.VISIBLE);
            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(ApiRequest.DOG_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiRequest request=retrofit.create(ApiRequest.class);
            Call<Animals> call=request.getDog();
            call.enqueue(new Callback<Animals>() {
                @Override
                public void onResponse(Call<Animals> call, Response<Animals> response) {
                    if (response.isSuccessful() && response.body()!=null){
                        animals = response.body();
                        String imageUrl=animals.getDogUrl();
                        Picasso.get().load(imageUrl).into(photoView);
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<Animals> call, Throwable t) {
                    Toast.makeText(MainActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                }
            });
        }
        else
            Toasty.error(context, "You're offline!", Toast.LENGTH_SHORT, true).show();
    }

    //initialize views function
    private void initViews() {
        btnFox=findViewById(R.id.btn_fox);
        btnCat=findViewById(R.id.btn_cat);
        btnDog=findViewById(R.id.btn_dog);
        btnSave=findViewById(R.id.btnSave);
        btnAddFavourites=findViewById(R.id.btn_add_favourite);
        btnFavourites=findViewById(R.id.btn_favorites);
        photoView=findViewById(R.id.photoView);
        progressBar=findViewById(R.id.progress_bar);
        toolbar=findViewById(R.id.tool_bar);
    }

    //function for get read/write permission
    private boolean getStoragePermission() {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED)
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
            return false;
        }
        else {
            return true;
        }
    }

    //toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.favouriteItem) {
            Intent intent = new Intent(context, FavouritesActivity.class);
            startActivity(intent);
        }
        else if (id==R.id.exitItem)
        {
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Alert!")
                    .setMessage("Do you want to exit?")
                    .setPositiveButton("Yes", (dialog, which) -> finish())
                    .setNegativeButton("No", (dialog, which) -> {

                    });
            AlertDialog alertDialog=builder.create();
            alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    //Convert bitmap to byteArray
    private byte[] photoViewToByte(PhotoView image) {
        Bitmap bitmap=((BitmapDrawable) image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] byteArray=stream.toByteArray();
        return byteArray;
    }
}