package ir.nasim.animalsgallery_retrofit.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ir.nasim.animalsgallery_retrofit.Adapter.ImagesAdapter;
import ir.nasim.animalsgallery_retrofit.Model.Images;
import ir.nasim.animalsgallery_retrofit.R;

public class FavouritesActivity extends AppCompatActivity {

    private RecyclerView imageRecycler;
    private Context context;
    private ImagesAdapter adapter;
    private List<Images> images=new ArrayList<>();
    private Toolbar favouritesToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);

        context=this;

        favouritesToolbar=findViewById(R.id.favourites_toolbar);
        setSupportActionBar(favouritesToolbar);
        //set back button on toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        favouritesToolbar.setNavigationOnClickListener(view -> finish());

        imageRecycler=findViewById(R.id.image_recycler);

        adapter=new ImagesAdapter(images,context);
        imageRecycler.setLayoutManager(new GridLayoutManager(context,2,GridLayoutManager.VERTICAL,true));
        imageRecycler.setAdapter(adapter);

        List<Images> getImages;
        getImages=MainActivity.database.imageDao().getAll();

        for (int i = 0; i < getImages.size(); i++) {
            images.add(new Images(getImages.get(i).getImageUrl()));
            adapter.notifyDataSetChanged();
            //add new item top
            adapter.notifyItemRangeInserted(0,images.size());
            imageRecycler.scrollToPosition(images.size()-1);
        }

    }
}