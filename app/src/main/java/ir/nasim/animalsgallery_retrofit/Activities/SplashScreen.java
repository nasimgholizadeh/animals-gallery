package ir.nasim.animalsgallery_retrofit.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import ir.nasim.animalsgallery_retrofit.R;

public class SplashScreen extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH=3000;

    private ImageView splashIcon;
    private TextView splashText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        splashIcon =findViewById(R.id.splashScreenIcon);
        alphaAnimation();

        splashText=findViewById(R.id.splashScreenText);
        translateAnimation();

        new Handler().postDelayed(() -> {
            Intent intent=new Intent(SplashScreen.this,MainActivity.class);
            SplashScreen.this.startActivity(intent);
            SplashScreen.this.finish();
        },SPLASH_DISPLAY_LENGTH);
    }

    //alpha animation
    private void alphaAnimation() {
        AlphaAnimation alphaAnimation=new AlphaAnimation(0.0f,1.0f);
        alphaAnimation.setDuration(2000);
        alphaAnimation.setFillAfter(true);
        splashIcon.startAnimation(alphaAnimation);
    }

    //translate animation
    private void translateAnimation() {
        TranslateAnimation translateAnimation=new TranslateAnimation(0,0,500,0);
        translateAnimation.setDuration(2000);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        splashText.startAnimation(translateAnimation);
    }
}