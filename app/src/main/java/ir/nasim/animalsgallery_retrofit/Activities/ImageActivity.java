package ir.nasim.animalsgallery_retrofit.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import ir.nasim.animalsgallery_retrofit.BitmapHelper;
import ir.nasim.animalsgallery_retrofit.R;
import uk.co.senab.photoview.PhotoView;

public class ImageActivity extends AppCompatActivity {

    private PhotoView photoView;
    private Button btnSave;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        photoView=findViewById(R.id.photoView);
        //get image
        photoView.setImageBitmap(BitmapHelper.getInstance().getBitmap());

        btnSave=findViewById(R.id.btnSave);
        btnSave.setOnClickListener(view -> {
            getStoragePermission();

            bitmap = ((BitmapDrawable) photoView.getDrawable()).getBitmap();

            String time = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(System.currentTimeMillis());
            File path = Environment.getExternalStorageDirectory();
            File dir = new File(path + "/DCIM/Animals");
            dir.mkdirs();
            String imageName = time + ".jpg";
            File file = new File(dir, imageName);
            OutputStream out;
            try {
                out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
                Toasty.success(ImageActivity.this,"Saved.", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toasty.error(ImageActivity.this,"An error occurred in storage!!!",Toast.LENGTH_LONG).show();
            }
            //show image in gallery
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(file));
            sendBroadcast(intent);
        });
    }

    //function for get read/write permission
    private boolean getStoragePermission() {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED)
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
            return false;
        }
        else {
            return true;
        }
    }
}