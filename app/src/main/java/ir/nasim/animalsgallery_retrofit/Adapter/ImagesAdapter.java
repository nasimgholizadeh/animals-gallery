package ir.nasim.animalsgallery_retrofit.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.nasim.animalsgallery_retrofit.BitmapHelper;
import ir.nasim.animalsgallery_retrofit.Activities.ImageActivity;
import ir.nasim.animalsgallery_retrofit.Model.Images;
import ir.nasim.animalsgallery_retrofit.R;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImagesViewHolder> {

    private List<Images> images;
    private Context context;

    public ImagesAdapter(List<Images> images, Context context) {
        this.images = images;
        this.context = context;
    }

    @NonNull
    @Override
    public ImagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.images_layout,parent,false);
        return new ImagesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesViewHolder holder, int position) {
        Images favoriteImages=images.get(position);
        byte[] favouriteImage=favoriteImages.getImageUrl();
        Bitmap bitmap= BitmapFactory.decodeByteArray(favouriteImage,0,favouriteImage.length);
        holder.imageView.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ImagesViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;
        private CardView cardView;
        public ImagesViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView=itemView.findViewById(R.id.imageView);
            cardView=itemView.findViewById(R.id.card_view);

            cardView.setOnClickListener(view -> {
                //send bitmap to ImageActivity
                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                BitmapHelper.getInstance().setBitmap(bitmap);
                Intent intent=new Intent(view.getContext(), ImageActivity.class);
                view.getContext().startActivity(intent);
            });
        }
    }
}
