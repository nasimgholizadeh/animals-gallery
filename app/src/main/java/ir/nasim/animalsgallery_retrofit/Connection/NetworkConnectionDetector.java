package ir.nasim.animalsgallery_retrofit.Connection;

import android.app.Service;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnectionDetector {
    private Context context;

    public NetworkConnectionDetector(Context context) {
        this.context = context;
    }

    public boolean isConnected() {
        ConnectivityManager connectivityManager= (ConnectivityManager) context.getSystemService(Service.CONNECTIVITY_SERVICE);
        if (connectivityManager!=null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info!=null) {
                if (info.getState()== NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }
}
