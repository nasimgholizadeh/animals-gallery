package ir.nasim.animalsgallery_retrofit.Model;

import com.google.gson.annotations.SerializedName;

public class Animals {
    @SerializedName("image")
    private String foxUrl;
    @SerializedName("file")
    private String catUrl;
    @SerializedName("message")
    private String dogUrl;

    public String getDogUrl() {
        return dogUrl;
    }

    public void setDogUrl(String dogUrl) {
        this.dogUrl = dogUrl;
    }

    public String getCatUrl() {
        return catUrl;
    }

    public void setCatUrl(String catUrl) {
        this.catUrl = catUrl;
    }

    public String getFoxUrl() {
        return foxUrl;
    }

    public void setFoxUrl(String foxUrl) {
        this.foxUrl = foxUrl;
    }
}
